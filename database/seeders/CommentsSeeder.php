<?php

namespace Database\Seeders;

use App\Models\Comment;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;

class CommentsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $response = Http::get('https://jsonplaceholder.typicode.com/comments');
        $comments = $response->json();
        foreach ($comments as $comment) {
            $currentDateTime = Carbon::now()->toDateTimeString();
            Comment::create([
                'user_id' => rand(1, 10),
                'post_id' => rand(1, 20),
                'content' => $comment['body'],
                'published_at' => $currentDateTime,
            ]);
        }
    }
}
