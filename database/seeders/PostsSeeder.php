<?php

namespace Database\Seeders;

use App\Models\Post;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Http;

class PostsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $i = 1;
        while ($i <= 30) {
            $response = Http::get('https://jsonplaceholder.typicode.com/posts/'.$i);
            $postData = $response->json();
            $currentDateTime = Carbon::now()->toDateTimeString();
            $post = Post::create([
                'user_id' => $postData['userId'],
                'title' => $postData['title'],
                'content' => $postData['body'],
                'image' => null,
                'published_at' => $currentDateTime,
            ]);
            $post->topics()->sync(array('11'));
            $i++;
        }
//        $response = Http::get('https://jsonplaceholder.typicode.com/posts');
//        $posts = $response->json();
//        foreach ($posts as $postData) {
//            $currentDateTime = Carbon::now()->toDateTimeString();
//            $post = Post::create([
//                'user_id' => $postData['userId'],
//                'title' => $postData['title'],
//                'content' => $postData['body'],
//                'image' => null,
//                'published_at' => $currentDateTime,
//            ]);
//            $post->topics()->sync(array('11'));
//        }
    }
}
