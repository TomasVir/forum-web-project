<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Guard;

class RolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'admin',
            'guard_name' => Guard::getDefaultName(static::class),
        ]);

        DB::table('roles')->insert([
            'name' => 'member',
            'guard_name' => Guard::getDefaultName(static::class),
        ]);
    }
}
