<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::create([
            'name' => 'admin',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('admin123'),
        ]);
        $user->assignRole('admin');

        $i = 1;
        while ($i < 10) {
            $name = 'user'.$i;
            $email = 'user'.$i.'@gmail.com';

            $user = User::create([
                'name' => $name,
                'email' => $email,
                'password' => Hash::make('user123'),
            ]);
            $user->assignRole('member');
            $i++;
        }
    }
}
