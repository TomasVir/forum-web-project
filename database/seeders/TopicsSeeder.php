<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TopicsSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $items = [
            ['title' => 'Sports'],
            ['title' => 'Health'],
            ['title' => 'Music'],
            ['title' => 'Video games'],
            ['title' => 'Board games'],
            ['title' => 'Movies'],
            ['title' => 'History'],
            ['title' => 'Art'],
            ['title' => 'Technologies'],
            ['title' => 'Cooking'],
            ['title' => 'Other'],
        ];
        foreach ($items as $item) {
            DB::table('topics')->insert($item);
        }
    }
}
