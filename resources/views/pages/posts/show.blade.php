@extends('layouts.app')

@section('title', 'Post')

@section('content')
<div class="col-lg-8">
    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade show mt-6" role="alert">
        {{ Session::get('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        @php
        Session::forget('success');
        @endphp
    </div>
    @endif
    <!-- Title-->
    <h1 class="mt-4">{{ $post->title }}</h1>
    <!-- Author-->
    <p class="lead">
        by
        <span class="font-bold">{{ $post->user->name }}</span>
    </p>
    <hr />
    <!-- Date and time-->
    <p>Posted on {{ date('jS M Y', strtotime($post->published_at)) }} at {{ date('h:i A',
        strtotime($post->published_at)) }}</p>
    <hr />
    <div class="flex items-center flex-wrap mb-2">
        @foreach($post->topics as $topic)
        <div class="p-1 border border-gray-300 w-fit mr-2 bg-gray-100 rounded mb-2">
            <span>{{ $topic->title }}</span>
        </div>
        @endforeach
    </div>
    <!-- Preview image-->
    @if(isset($post->image))
    <img class="img-fluid rounded w-full" src="{{ url('uploads/images', $post->image) }}" alt="">
    @else
    <img class="img-fluid rounded w-full" src="{{ url('img/placeholder-bg.jpg') }}" alt="">
    @endif
    <hr />
    <!-- Post content-->
    <p class="lead">{{ $post->content }}</p>
    @if(isset(Auth::user()->id) && (Auth::user()->id == $post->user_id || Auth::user()->hasRole('admin')))
    <a href="{{ url('posts/'.$post->id.'/edit') }}" class="btn btn-default ml-auto"><i class="fas fa-edit"></i>Edit</a>
    {!! Form::open(['method'=>'DELETE', 'url' => ['posts', $post->id], 'style' => 'display:inline']) !!}
    {!! Form::button('<i class="fas fa-trash-alt"></i> Delete', ['class' => 'btn btn-danger', 'type' => 'submit']) !!}
    {!! Form::close() !!}
    @endif
    <hr />
    @if(Auth::check())
    <!-- Comments form-->
    <div class="card my-4">
        {!! Form::open(['method'=>'POST', 'url' => ['posts', $post->id, 'comment']]) !!}
        {!! Form::label('content', 'Leave a Comment: ', ['class' => 'card-header w-full']) !!}
        <div class="card-body">
            <div class="mb-4">
                {!! Form::textarea('content', null, ['class' => 'form-control'.($errors->has('content') ? ' is-invalid' : ''), 'rows' => 3]) !!}
                {!! $errors->first('content', '<div class="invalid-feedback">:message</div>') !!}
            </div>
            {!! Form::button('Submit', ['class' => 'btn btn-primary', 'type' => 'submit']) !!}
        </div>
        {!! Form::close() !!}
    </div>
    @endif
    <!-- Single comment-->
    @foreach($post->comments as $comment)
    <div class="media mb-4">
        <img class="d-flex mr-3 rounded-circle h-14" src="{{ url('img/avatar-placeholder.png') }}" alt="avatar" />
        <div class="media-body">
            <h5 class="mt-0">{{ $comment->user->name }} <small>Commented on {{ date('jS M Y', strtotime($comment->published_at)) }} at {{ date('h:i A',
                    strtotime($comment->published_at)) }}</small>

                @if(isset(Auth::user()->id) && (Auth::user()->id == $comment->user_id || Auth::user()->hasRole('admin')))
                {!! Form::open(['method'=>'POST', 'url' => ['comment', $comment->id, 'delete'], 'style' => 'display:inline']) !!}
                {!! Form::button('<i class="fas fa-trash-alt"></i> Delete', ['class' => 'btn btn-danger btn-sm', 'type' => 'submit']) !!}
                {!! Form::close() !!}
                @endif
            </h5>
            {{ $comment->content }}
        </div>
    </div>
    @endforeach
</div>
<!-- Side widgets-->
<div class="col-md-4">
    <!-- Search widget-->
    <div class="card my-4">
        <h5 class="card-header">Search</h5>
        <div class="card-body">
            {!! Form::open(['method'=>'GET', 'url' => ['posts'], 'style' => 'display:flex;']) !!}
            {!! Form::text('q', null, ['class' => 'form-control'.($errors->has('q') ? ' is-invalid' : ''), 'placeholder' => 'Search by title...', 'required' => 'required']) !!}
            {!! Form::button('Go!', ['class' => 'btn btn-secondary', 'type' => 'submit']) !!}
            {!! Form::close() !!}
        </div>
    </div>
    <!-- Categories widget-->
    <div class="card my-4">
        <h5 class="card-header">Topics</h5>
        <div class="card-body">
            <div class="flex flex-wrap">
                @foreach($topics as $topic)
                <a class="mr-4" href="{{ url('posts/topic/'.$topic->id) }}">{{ $topic->title }}</a>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
