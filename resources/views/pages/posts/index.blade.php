@extends('layouts.app')

@section('title', 'Home')

@section('content')
<!-- Blog entries-->
<div class="col-md-8">
    <h1 class="my-4">
        Home
        <small>All posts</small>
    </h1>

    @if(Session::has('success'))
    <div class="alert alert-success alert-dismissible fade show" role="alert">
        {{ Session::get('success') }}
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
        @php
        Session::forget('success');
        @endphp
    </div>
    @endif

    @if(Auth::check())
        <a class="btn btn-success mb-6" href="{{ url('/posts/create') }}">Create post</a>
    @endif

    @if(count($posts) == 0)
    <h4>No posts found.</h4>
    @endif

    @foreach($posts as $post)
    <!-- Blog post-->
    <div class="card mb-4">
        @if(isset($post->image))
        <img class="card-img-top object-cover mt-2" src="{{ url('uploads/images', $post->image) }}" alt="" height="100" style="height: 350px;">
        @else
        <img class="card-img-top object-cover mt-2" src="{{ url('img/placeholder-bg.jpg') }}" alt="" height="100" style="height: 350px;">
        @endif
        <div class="card-body">
            <div class="flex items-center flex-wrap mb-2">
                @foreach($post->topics as $topic)
                <div class="p-1 border border-gray-300 w-fit mr-2 bg-gray-100 rounded mb-2">
                    <span>{{ $topic->title }}</span>
                </div>
                @endforeach
            </div>
            <h2 class="card-title">{{ $post->title }}</h2>
            <p class="card-text text-truncate">{{ $post->content }}</p>
            <a class="btn btn-primary" href="{{ url('posts/'.$post->id) }}">Read More →</a>
            @if(isset(Auth::user()->id) && (Auth::user()->id == $post->user_id || Auth::user()->hasRole('admin')))
            <a href="{{ url('posts/'.$post->id.'/edit') }}" class="btn btn-default ml-auto"><i class="fas fa-edit"></i>Edit</a>
            {!! Form::open(['method'=>'DELETE', 'url' => ['posts', $post->id], 'style' => 'display:inline']) !!}
            {!! Form::button('<i class="fas fa-trash-alt"></i> Delete', ['class' => 'btn btn-danger', 'type' => 'submit']) !!}
            {!! Form::close() !!}
            @endif
        </div>
        <div class="card-footer text-muted">
            Posted on {{ date('jS M Y', strtotime($post->published_at)) }} by
            <span class="font-bold">{{ $post->user->name }}</span>
        </div>
    </div>
    @endforeach
    <!-- Pagination-->
    <div class="mb-6">
        {{ $posts->links() }}
    </div>
</div>
<!-- Side widgets-->
<div class="col-md-4">
    <!-- Search widget-->
    <div class="card my-4">
        <h5 class="card-header">Search</h5>
        <div class="card-body">
            {!! Form::open(['method'=>'GET', 'url' => ['posts'], 'style' => 'display:flex;']) !!}
            {!! Form::text('q', null, ['class' => 'form-control'.($errors->has('q') ? ' is-invalid' : ''), 'placeholder' => 'Search by title...', 'required' => 'required']) !!}
            {!! Form::button('Go!', ['class' => 'btn btn-secondary', 'type' => 'submit']) !!}
            {!! Form::close() !!}
        </div>
    </div>
    <!-- Categories widget-->
    <div class="card my-4">
        <h5 class="card-header">Topics</h5>
        <div class="card-body">
            <div class="flex flex-wrap">
                @foreach($topics as $topic)
                <a class="mr-4" href="{{ url('posts/topic/'.$topic->id) }}">{{ $topic->title }}</a>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
