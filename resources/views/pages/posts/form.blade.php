@extends('layouts.app')

@section('title', 'Create post')

@section('content')
@if(isset($post) && isset(Auth::user()->id) && Auth::user()->id != $post->user_id && !Auth::user()->hasRole('admin'))
<div class="my-6">
    <h1>You don't have rights to edit this post!</h1>
</div>
@else
<!-- Blog entries-->
<div class="col-md-12">
    <h1 class="my-4 text-2xl">
        @if(isset($post))
        Edit post
        @else
        Create post
        @endif
        </h1>

    @if(isset($post))
    {!! Form::model($post, ['url' => ['posts', $post->id], 'method' => 'patch', 'class' => 'needs-validation', 'enctype' => 'multipart/form-data']) !!}
    @else
    {!! Form::open(['url' => 'posts', 'class' => 'needs-validation', 'enctype' => 'multipart/form-data']) !!}
    @endif

    <div class="form-group mb-4">
        {!! Form::label('title', 'Title: ', ['class' => 'col-sm-3']) !!}
        <div class="col-sm-6">
            {!! Form::text('title', null, ['class' => 'form-control'.($errors->has('title') ? ' is-invalid' : '')]) !!}
            {!! $errors->first('title', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>
    <div class="form-group mb-4">
        {!! Form::label('content', 'Content: ', ['class' => 'col-sm-3']) !!}
        <div class="col-sm-6">
            {!! Form::textarea('content', null, ['class' => 'form-control'.($errors->has('content') ? ' is-invalid' : '')]) !!}
            {!! $errors->first('content', '<div class="invalid-feedback">:message</div>') !!}
        </div>
    </div>
    <div class="form-group mb-4">
        {!! Form::label('topic_id', 'Topics: ', ['class' => 'col-sm-3']) !!}
        <div class="col-sm-6">
            {!!Form::select('topic_id', $topics, isset($selected_topics) ? $selected_topics : null, ['class' => 'form-control h-48'.($errors->has('topics') ? ' is-invalid' : ''), 'name' => 'topics[]', 'multiple' => 'multiple'])!!}
            {!! $errors->first('topics', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        <p class="col-sm-3">To select multiple topics hold CTRL</p>
    </div>
    <div class="form-group mb-4 col-sm-6">
        <div class="custom-file">
            {!! Form::label('image', isset($post->image) ? $post->image : 'Upload post image', ['class' => 'custom-file-label'.($errors->has('image') ? ' is-invalid' : '')]) !!}
            {!! Form::file('image', ['class' => 'custom-file-input']) !!}
            <script>
                $(".custom-file-input").on("change", function() {
                    var fileName = $(this).val().split("\\").pop();
                    $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                    var file = $(this).get(0).files[0];
                    if(file) {
                        var reader = new FileReader();

                        reader.onload = function(){
                            $("#previewImage").attr("src", reader.result);
                            $("#previewImageDefault").attr("src", reader.result);
                        }

                        reader.readAsDataURL(file);
                    }
                });
            </script>
            {!! $errors->first('image', '<div class="invalid-feedback">:message</div>') !!}
        </div>
        @if(isset($post->image))
        <img id="previewImage" src="{{ url('uploads/images', $post->image) }}" height="100" class="mt-2" alt="">
        @else
        <img id="previewImageDefault" src="{{ url('img/placeholder-bg.jpg') }}" height="100" class="mt-2" alt="">
        @endif
    </div>
    <div class="form-group mb-4">
        <div class="col-sm-offset-3 col-sm-3">
            {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
        </div>
    </div>
    {!! Form::close() !!}
</div>
@endif
@endsection
