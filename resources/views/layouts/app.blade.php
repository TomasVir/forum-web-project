<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="keywords" content="library, books, writers">
    <meta name="description" content="In this library you can find many new and old books.">
    <meta name="author" content="">
    <title>{{ config('app.name', 'Forum') }}</title>

    <link href="" rel="shortcut icon">
    <link href="{{ asset('bower_components/bootstrap/dist/css/bootstrap.min.css') }}" rel="stylesheet">
    <link href="{{ asset('bower_components/fontawesome/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/adminlte.min.css') }}" rel="stylesheet">
    <script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ mix('js/app.js') }}" defer></script>

</head>


<body class="body-wrapper">
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name', 'Forum') }}</a>
        <button id="navbarResponsiveButton" class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="{{ url('/') }}">
                        Posts
                        <span class="sr-only">(current)</span>
                    </a>
                </li>
                @guest
                <li class="nav-item">
                    <a class="nav-link login-button" href="{{ route('login') }}">{{ __('Login') }}</a>
                </li>
                @if (Route::has('register'))
                <li class="nav-item">
                    <a class="nav-link login-button" href="{{ route('register') }}">{{ __('Register') }}</a>
                </li>
                @endif
                @else
                <li class="nav-item dropdown">
                    <a id="navbarDropdown" class="nav-link dropdown-toggle" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                        {{ Auth::user()->name }} <span class="caret"></span>
                    </a>

                    <div id="navbarDropdownContent" class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                        @role('admin')
                        <a class="dropdown-item" href="{{ url('admin') }}">
                            Admin panel
                        </a>
                        @endrole
                        <a class="dropdown-item" href="{{ route('profile.show') }}">
                            Profile
                        </a>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            {{ __('Logout') }}
                        </a>

                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                </li>
                @endguest
            </ul>
        </div>
        <script type="text/javascript">
            $("#navbarDropdown").click(() => {
                const navbarResponsive = $("#navbarDropdownContent");
                if (navbarResponsive.hasClass('block')) {
                    navbarResponsive.removeClass('block');
                } else {
                    navbarResponsive.addClass('block');
                }
            });
            $("#navbarResponsiveButton").click(() => {
               const navbarResponsive = $("#navbarResponsive");
               if (navbarResponsive.hasClass('block')) {
                   navbarResponsive.removeClass('block');
                   navbarResponsive.removeClass('h-auto');
                   navbarResponsive.addClass('h-0');
               } else {
                   navbarResponsive.addClass('block');
                   navbarResponsive.removeClass('h-0');
                   navbarResponsive.addClass('h-auto');
               }
            });
        </script>
    </div>
</nav>
<section>
    <div class="container">
        <div class="row">
            @yield('content')
        </div>
    </div>
</section>

@if(isset($slot))
@if (isset($header))
<header class="bg-white shadow">
    <div class="max-w-7xl mx-auto py-6 px-4 sm:px-6 lg:px-8">
        {{ $header }}
    </div>
</header>
@endif
{{ $slot }}
@stack('modals')
@livewireScripts
@endif

<footer class="footer-bottom">
    <!-- Container Start -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <!-- Copyright -->
                <div class="copyright">
                    <p>Copyright © <script>
                            var CurrentYear = new Date().getFullYear()
                            document.write(CurrentYear)
                        </script>. All Rights Reserved, theme by <a class="text-primary" href="https://themefisher.com" target="_blank">themefisher.com</a></p>
                </div>
            </div>
        </div>
    </div>
    <!-- Container End -->
</footer>

<!-- JavaScripts -->
<script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('bower_components/fontawesome/js/all.min.js') }}"></script>

</body>

</html>
