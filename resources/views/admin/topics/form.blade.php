@extends('layouts.admin')

@section('title', 'Topics')

@section('content')
<div class="card">
    <div class="card-header">
        <h6 class="m-0 font-weight-bold text-primary">
            @if(isset($topic))
            Edit existing topic
            @else
            Create new topic
            @endif
        </h6>
    </div>
    <div class="card-body">
        @if(isset($topic))
        {!! Form::model($topic, ['url' => ['admin/topics', $topic->id], 'method' => 'patch', 'class' => 'needs-validation']) !!}
        @else
        {!! Form::open(['url' => 'admin/topics', 'class' => 'needs-validation']) !!}
        @endif

        <div class="form-group">
            {!! Form::label('title', 'Title: ', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-6">
                {!! Form::text('title', null, ['class' => 'form-control'.($errors->has('title') ? ' is-invalid' : '')]) !!}
                {!! $errors->first('title', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-3">
                {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
