@extends('layouts.admin')

@section('title', 'Post')

@section('content')
<div class="card">
    <div class="card-header">
        <a href="{{ url('/admin/posts/'.$post->id.'/edit') }}" class="btn btn-primary"><i class="fas fa-edit"></i> Edit post</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td>ID</td>
                    <td>{{ $post->id }}</td>
                </tr>
                <tr>
                    <td>Title</td>
                    <td>{{ $post->title }}</td>
                </tr>
                <tr>
                    <td>Content</td>
                    <td>{{ $post->content }}</td>
                </tr>
                <tr>
                    <td>Published by</td>
                    <td>
                        <a href="{{ url('admin/users/'.$post->user_id) }}">{{ $post->user->name }}</a>
                    </td>
                </tr>
                <tr>
                    <td>Post image</td>
                    <td>
                        @if(isset($post->image))
                        <img src="{{ url('uploads/images', $post->image) }}" height="100" alt="">
                        @else
                        <img src="{{ url('img/placeholder-bg.jpg') }}" height="100" alt="">
                        @endif
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
