@extends('layouts.admin')

@section('title', 'Posts')

@section('content')
<div class="card">
    <div class="card-header">
        <h6 class="m-0 font-weight-bold text-primary">
            @if(isset($post))
            Edit existing post
            @else
            Create new post
            @endif
        </h6>
    </div>
    <div class="card-body">
        @if(isset($post))
        {!! Form::model($post, ['url' => ['admin/posts', $post->id], 'method' => 'patch', 'class' => 'needs-validation', 'enctype' => 'multipart/form-data']) !!}
        @else
        {!! Form::open(['url' => 'admin/posts', 'class' => 'needs-validation', 'enctype' => 'multipart/form-data']) !!}
        @endif

        <div class="form-group">
            {!! Form::label('title', 'Title: ', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-6">
                {!! Form::text('title', null, ['class' => 'form-control'.($errors->has('title') ? ' is-invalid' : '')]) !!}
                {!! $errors->first('title', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('content', 'Content: ', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-6">
                {!! Form::textarea('content', null, ['class' => 'form-control'.($errors->has('content') ? ' is-invalid' : '')]) !!}
                {!! $errors->first('content', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </div>
        @if(!isset($post))
        <div class="form-group">
            {!! Form::label('user_id', 'User: ', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-6">
                {!!Form::select('user_id', $users, null, ['class' => 'form-control'])!!}
                {!! $errors->first('user_id', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </div>
        @endif
        <div class="form-group">
            {!! Form::label('topic_id', 'Topics: ', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-6">
                {!!Form::select('topic_id', $topics, isset($selected_topics) ? $selected_topics : null, ['class' => 'form-control h-48'.($errors->has('topics') ? ' is-invalid' : ''), 'name' => 'topics[]', 'multiple' => 'multiple'])!!}
                {!! $errors->first('topics', '<div class="invalid-feedback">:message</div>') !!}
            </div>
            <p class="col-sm-3">To select multiple topics hold CTRL</p>
        </div>
        <div class="form-group col-sm-6">
            <div class="custom-file">
                {!! Form::label('image', isset($post->image) ? $post->image : 'Upload post image', ['class' => 'custom-file-label'.($errors->has('image') ? ' is-invalid' : '')]) !!}
                {!! Form::file('image', ['class' => 'custom-file-input']) !!}
                <script>
                    $(".custom-file-input").on("change", function() {
                        var fileName = $(this).val().split("\\").pop();
                        $(this).siblings(".custom-file-label").addClass("selected").html(fileName);
                        var file = $(this).get(0).files[0];
                        if(file) {
                            var reader = new FileReader();

                            reader.onload = function(){
                                $("#previewImage").attr("src", reader.result);
                                $("#previewImageDefault").attr("src", reader.result);
                            }

                            reader.readAsDataURL(file);
                        }
                    });
                </script>
                {!! $errors->first('image', '<div class="invalid-feedback">:message</div>') !!}
            </div>
            @if(isset($post->image))
            <img id="previewImage" src="{{ url('uploads/images', $post->image) }}" height="100" class="mt-2" alt="">
            @else
            <img id="previewImageDefault" src="{{ url('img/placeholder-bg.jpg') }}" height="100" class="mt-2" alt="">
            @endif
        </div>
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-3">
                {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
