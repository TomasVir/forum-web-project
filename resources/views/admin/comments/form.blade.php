@extends('layouts.admin')

@section('title', 'Comments')

@section('content')
<div class="card">
    <div class="card-header">
        <h6 class="m-0 font-weight-bold text-primary">
            @if(isset($comment))
            Edit existing comment
            @else
            Create new comment
            @endif
        </h6>
    </div>
    <div class="card-body">
        @if(isset($comment))
        {!! Form::model($comment, ['url' => ['admin/comments', $comment->id], 'method' => 'patch', 'class' => 'needs-validation']) !!}
        @else
        {!! Form::open(['url' => 'admin/comments', 'class' => 'needs-validation']) !!}
        @endif

        <div class="form-group">
            {!! Form::label('content', 'Content: ', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-6">
                {!! Form::textarea('content', null, ['class' => 'form-control'.($errors->has('content') ? ' is-invalid' : '')]) !!}
                {!! $errors->first('content', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </div>
        @if(!isset($comment))
        <div class="form-group">
            {!! Form::label('user_id', 'User: ', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-6">
                {!!Form::select('user_id', $users, null, ['class' => 'form-control'])!!}
                {!! $errors->first('user_id', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </div>
        <div class="form-group">
            {!! Form::label('post_id', 'Post: ', ['class' => 'col-sm-3']) !!}
            <div class="col-sm-6">
                {!!Form::select('post_id', $posts, null, ['class' => 'form-control'])!!}
                {!! $errors->first('post_id', '<div class="invalid-feedback">:message</div>') !!}
            </div>
        </div>
        @endif
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-3">
                {!! Form::submit('Save', ['class' => 'btn btn-primary form-control']) !!}
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>
@endsection
