@extends('layouts.admin')

@section('title', 'Comment')

@section('content')
<div class="card">
    <div class="card-header">
        <a href="{{ url('/admin/comments/'.$comment->id.'/edit') }}" class="btn btn-primary"><i class="fas fa-edit"></i> Edit comment</a>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered">
                <tbody>
                <tr>
                    <td>ID</td>
                    <td>{{ $comment->id }}</td>
                </tr>
                <tr>
                    <td>Content</td>
                    <td>{{ $comment->content }}</td>
                </tr>
                <tr>
                    <td>Post</td>
                    <td>
                        <a href="{{ url('admin/posts/'.$comment->post_id) }}">{{ $comment->post->title }}</a>
                    </td>
                </tr>
                <tr>
                    <td>Published by</td>
                    <td>
                        <a href="{{ url('admin/users/'.$comment->user_id) }}">{{ $comment->user->name }}</a>
                    </td>
                </tr>
                <tr>
                    <td>Published at</td>
                    <td>{{ $comment->published_at }}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
