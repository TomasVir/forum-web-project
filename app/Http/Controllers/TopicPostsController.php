<?php

namespace App\Http\Controllers;

use App\Models\Topic;
use Illuminate\Http\Request;

class TopicPostsController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($id, Request $request)
    {
        $topic = Topic::findOrFail($id);
        $topics = Topic::all();
        $posts = $topic->posts()->orderBy('published_at', 'DESC')->paginate(10);
        return view('pages.posts.topic-posts', compact('topic', 'topics', 'posts'));
    }
}
