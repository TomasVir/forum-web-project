<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CommentCreateController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($id, Request $request)
    {
        $request->validate([
            'content' => 'required',
        ]);

        $input = $request->all();
        $currentDateTime = Carbon::now()->toDateTimeString();

        Comment::create([
            'user_id' => auth()->user()->id,
            'post_id' => $id,
            'content' => $input['content'],
            'published_at' => $currentDateTime,
        ]);

        return redirect('posts/'.$id)->with('success', 'Comment added successfully.');
    }
}
