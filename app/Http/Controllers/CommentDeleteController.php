<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Illuminate\Http\Request;

class CommentDeleteController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke($id, Request $request)
    {
        $comment = Comment::findOrFail($id);
        $postId = $comment->post->id;
        $comment->delete();
        return redirect('posts/'.$postId)->with('success', 'Comment deleted successfully.');
    }
}
