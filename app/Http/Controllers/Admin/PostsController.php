<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Post;
use App\Models\Topic;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $posts = Post::paginate(10);
        return view('admin.posts.index', compact('posts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $users = User::pluck('email', 'id');
        $users->all();

        $topics = Topic::pluck('title', 'id');
        $topics->all();


        return view('admin.posts.form', compact('users', 'topics'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'user_id' => 'required',
            'title' => 'required',
            'content' => 'required',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'topics' => 'required',
        ]);

        $input = $request->all();

        if ($request->hasFile('image')) {
            $fileName = time().'.'.$request->image->extension();
            $request->image->move(public_path('uploads/images'), $fileName);
            $input['image'] = $fileName;
        } else {
            $input['image'] = null;
        }

        $currentDateTime = Carbon::now()->toDateTimeString();
        $topics = $input['topics'];
        $post = Post::create([
            'user_id' => $input['user_id'],
           'title' => $input['title'],
           'content' => $input['content'],
           'image' => $input['image'],
           'published_at' => $currentDateTime,
        ]);
        $post->topics()->sync($topics);

        return redirect('admin/posts')->with('success', 'Post added successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = Post::findOrFail($id);
        return view('admin.posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Post::findOrFail($id);

        $topics = Topic::pluck('title', 'id');
        $topics->all();

        $selected_topics[] = array();
        foreach ($post->topics as $topic) {
            $selected_topics[] = $topic->id;
        }

        return view('admin.posts.form', compact('post', 'topics', 'selected_topics'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'content' => 'required',
            'image' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'topics' => 'required',
        ]);

        $data = $request->all();
        $post = Post::findOrFail($id);

        if ($request->hasFile('image')) {
            @unlink(public_path('uploads/images/').$post->image);
            $fileName = time().'.'.$request->image->extension();
            $request->image->move(public_path('uploads/images'), $fileName);
            $data['image'] = $fileName;
        }

        $topics = $data['topics'];

        $post->update($data);
        $post->topics()->sync($topics);

        return redirect('admin/posts')->with('success', 'Post updated successfully.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Post::findOrFail($id);
        @unlink(public_path('uploads/images/').$post->image);
        $post->comments()->delete();
        $post->topics()->detach();
        $post->delete();
        return redirect('admin/posts')->with('success', 'Post deleted successfully.');
    }
}
