<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
   return redirect('/posts');
});
Route::post('posts/{id}/comment', \App\Http\Controllers\CommentCreateController::class);
Route::post('comment/{id}/delete', \App\Http\Controllers\CommentDeleteController::class);
Route::get('posts/topic/{id}', \App\Http\Controllers\TopicPostsController::class);
Route::resource('/posts', App\Http\Controllers\HomeController::class);
Route::group(['middleware' => ['role:admin']], function () {
    Route::get('/admin', function () {
        return view('admin.dashboard');
    });

    Route::resource('admin/roles', App\Http\Controllers\Admin\RolesController::class);
    Route::resource('admin/users', App\Http\Controllers\Admin\UsersController::class);
    Route::resource('admin/topics', App\Http\Controllers\Admin\TopicsController::class);
    Route::resource('admin/posts', App\Http\Controllers\Admin\PostsController::class);
    Route::resource('admin/comments', App\Http\Controllers\Admin\CommentsController::class);
});
